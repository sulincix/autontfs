install:
	mkdir -p $(DESTDIR)/usr/bin/
	install autontfs.sh $(DESTDIR)/usr/bin/autontfs
	
install-systemd:
	mkdir -p $(DESTDIR)/etc/systemd/system/
	install systemd.service $(DESTDIR)/etc/systemd/system/autontfs.service
	
install-openrc:
	mkdir -p $(DESTDIR)/etc/init.d/
	install openrc.service $(DESTDIR)/etc/init.d/autontfs
