lsblk -J | grep part | grep -v '"/"' | cut -d '"' -f 4 | while read line
do
	TYPE=$(LANG=C blkid  /dev/$line | sed "s/.*TYPE=\"//g" | sed "s/\".*//g")
	echo "$line $TYPE"
	if [ $TYPE == "ntfs" ] ; then
		echo -e "\033[32;1m/dev/$line\033[;0m Type: $TYPE"
		ntfsfix /dev/$line || true
	elif [ "$(which fsck.$TYPE 2>/dev/null)" != "" ] ; then
		echo -e "\033[32;1m/dev/$line\033[;0m Type: $TYPE"
		umount /dev/$line
		fsck.$TYPE -y /dev/$line || true
	fi
done
